package com.example.test;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;
    private double currentPrice;
    private LocalDateTime timestamp;


    public Product( String name, double currentPrice, LocalDateTime timestamp) {
        this.name = name;
        this.currentPrice = currentPrice;
        this.timestamp = timestamp;
    }

    public Product() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public long getId() {
        return id;
    }

    public String toString() {
        return "Product name: " + this.name + "," + " Current price: " + this.currentPrice + "," + "Timestamp: " + this.timestamp;
    }

}
