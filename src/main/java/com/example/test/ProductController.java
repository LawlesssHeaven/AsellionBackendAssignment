package com.example.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
public class ProductController {

    private static final Logger log = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    ProductRepository repository;

    // Adds product to database
    @PostMapping(value = "/addProduct", consumes = "application/json", produces = "application/json")
    public Product createProduct(@RequestBody Product product) {
        LocalDateTime time = LocalDateTime.now();
        product.setTimestamp(time);
        log.info("Item saved to database" + product);
        return repository.save(product);
    }

    // Returns ArrayList of all products
    @RequestMapping("/getProducts")
    public ArrayList<Product> allProducts () {
        ArrayList<Product> myProducts = new ArrayList<>();
        log.info("All products in my database: ");
        for (Product product : repository.findAll()) {
            myProducts.add(product);
        }
         return  myProducts;
    }



}
